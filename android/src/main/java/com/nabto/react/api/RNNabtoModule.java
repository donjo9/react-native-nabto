package com.nabto.react.api;

import com.nabto.api.*;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableNativeArray;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import android.content.Context;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;
import android.R;
import android.util.Log;

public class RNNabtoModule extends ReactContextBaseJavaModule {
    private static final int NABTO_ERROR_MISSING_PREPARE = 2000068;
    private static final int GRACEPERIOD = 300; // seconds
    private NabtoApi nabto = null;
    private Session session;
    private Map<String, Tunnel> tunnels;

    private boolean adShown = false;
    private long timerStart = 0;
    // private AdService adService;
    private List<String> deviceCache;

    @Override
    public String getName() {
        return "Nabto";
    }

    public RNNabtoModule(ReactApplicationContext reactContext) {
        super(reactContext);
        deviceCache = new ArrayList<String>();
        // adService = new AdService();
        tunnels = new HashMap<String, Tunnel>();
    }

    @ReactMethod
    private void TestFunction(Promise promise) {
        promise.resolve("Test fungerer");
    }

    /**
     * Executes the request and returns PluginResult.
     *
     * @param action          The action to execute.
     * @param args            JSONArray of arguments for the plugin.
     * @param callbackContext The callback context used when calling back into
     *                        JavaScript.
     * @return True when the action was valid, false otherwise.
     */

    /* Nabto API */
    /*
     * private void prepareInvoke(final JSONArray jsonDevices, final CallbackContext
     * cc) { cordova.getThreadPool().execute(new Runnable() {
     * 
     * @Override public void run() { // call to the core asking if invoke is
     * prepared boolean showAdFlag = false; String dev; if(jsonDevices.length() <
     * 1){ cc.success(); Log.d("prepareInvoke",
     * "prepareInvoke was called with empty device list"); return; }
     * 
     * for (int i = 0; i< jsonDevices.length(); i++){ try{
     * //Log.d("prepareInvoke","jsonDevices[" + i + "]: " +
     * jsonDevices.get(i).toString()); dev = jsonDevices.get(i).toString();//
     * dev2.toString(); } catch (JSONException e){ Log.w(
     * "prepareInvoke","Nabto.java: Failed to get jsonDevice, bad JSON syntax, skipping device"
     * ); continue; }
     * 
     * // Checking if free, own-it or not AMP. We should agree how to define free
     * and own-it in url if (dev.matches("^[\\w]+\\.[\\w]{5}f(\\.[\\w]+)*$")){
     * Log.d("prepareInvoke","found free device: " + dev); showAdFlag = true; } else
     * { Log.d("prepareInvoke","found non-free device: " + dev); }
     * if(!deviceCache.contains(dev)){ deviceCache.add(dev); } }
     * 
     * if (timerStart != 0) { // an ad has been shown earlier - if within
     * graceperiod, do not show again if (System.currentTimeMillis()-timerStart <
     * GRACEPERIOD*1000){ Log.d("prepareInvoke","Invoking grace period"); adShown =
     * true; } } if(showAdFlag == true && adShown == false){
     * adService.showAd(cordova.getActivity(), webView.getContext()); timerStart =
     * System.currentTimeMillis(); adShown = true; } else {
     * //Log.d("prepareInvoke","Not showing ad, showAdFlag: " + showAdFlag +
     * " adShown: " + adShown); } cc.success(); } }); }
     */
    @ReactMethod
    private void prepareInvoke(final JSONArray jsonDevices, final Promise promise) {
        // call to the core asking if invoke is prepared
        boolean showAdFlag = false;
        String dev;
        if (jsonDevices.length() < 1) {
            promise.resolve(true);
            Log.d("prepareInvoke", "prepareInvoke was called with empty device list");
            return;
        }

        for (int i = 0; i < jsonDevices.length(); i++) {
            try {
                // Log.d("prepareInvoke","jsonDevices[" + i + "]: " +
                // jsonDevices.get(i).toString());
                dev = jsonDevices.get(i).toString();// dev2.toString();
            } catch (JSONException e) {
                Log.w("prepareInvoke", "Nabto.java: Failed to get jsonDevice, bad JSON syntax, skipping device");
                continue;
            }

            // Checking if free, own-it or not AMP. We should agree how to define free and
            // own-it in url
            if (dev.matches("^[\\w]+\\.[\\w]{5}f(\\.[\\w]+)*$")) {
                Log.d("prepareInvoke", "found free device: " + dev);
                showAdFlag = true;
            } else {
                Log.d("prepareInvoke", "found non-free device: " + dev);
            }
            if (!deviceCache.contains(dev)) {
                deviceCache.add(dev);
            }
        }
        /*
         * if (timerStart != 0) { // an ad has been shown earlier - if within
         * graceperiod, do not show again if (System.currentTimeMillis() - timerStart <
         * GRACEPERIOD * 1000) { Log.d("prepareInvoke", "Invoking grace period");
         * adShown = true; } } if (showAdFlag == true && adShown == false) {
         * adService.showAd(cordova.getActivity(), webView.getContext()); timerStart =
         * System.currentTimeMillis(); adShown = true; } else {
         * //Log.d("prepareInvoke","Not showing ad, showAdFlag: " + showAdFlag +
         * " adShown: " + adShown); }
         */
        promise.resolve(true);
        return;
    }

    /*
     * 
     * 
     * private void createSignedKeyPair(final String user, final String pass, final
     * CallbackContext cc) { cordova.getThreadPool().execute(new Runnable(){
     * 
     * @Override public void run() { if (nabto == null) {
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; } NabtoStatus
     * status = nabto.createProfile(user, pass); if (status != NabtoStatus.OK) {
     * cc.error(status.ordinal()); return; } cc.success(); } }); }
     * 
     */
    @ReactMethod
    private void createSignedKeyPair(final String user, final String pass, final Promise promise) {
        if (nabto == null) {
            promise.reject(Integer.toString(NabtoStatus.API_NOT_INITIALIZED.ordinal()),
                    String.valueOf(NabtoStatus.API_NOT_INITIALIZED));
                    return;
        }
        NabtoStatus status = nabto.createProfile(user, pass);
        if (status != NabtoStatus.OK) {
            promise.reject(Integer.toString(status.ordinal()), String.valueOf(status));
            return;
        }
        promise.resolve(true);
        return;
    }

    /*
     * private void createKeyPair(final String user, final String pass, final
     * CallbackContext cc) { cordova.getThreadPool().execute(new Runnable(){
     * 
     * @Override public void run() { if (nabto == null) {
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; } NabtoStatus
     * status = nabto.createSelfSignedProfile(user, pass); if (status !=
     * NabtoStatus.OK) { cc.error(status.ordinal()); return; } cc.success(); } });
     * 
     * }
     */
    @ReactMethod
    private void createKeyPair(final String user, final String pass, final Promise promise) {
        if (nabto == null) {
            promise.reject(Integer.toString(NabtoStatus.API_NOT_INITIALIZED.ordinal()),
                    String.valueOf(NabtoStatus.API_NOT_INITIALIZED));
                    return;
        }
        NabtoStatus status = nabto.createSelfSignedProfile(user, pass);
        if (status != NabtoStatus.OK) {
            promise.reject(Integer.toString(status.ordinal()), String.valueOf(status));
            return;
        }
        promise.resolve(true);
        return;
    }

    /*
     * private void removeKeyPair(final String user, final CallbackContext cc) {
     * cordova.getThreadPool().execute(new Runnable(){
     * 
     * @Override public void run() { if (nabto == null) {
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; } NabtoStatus
     * status = nabto.removeProfile(user); if (status != NabtoStatus.OK) {
     * cc.error(status.ordinal()); return; } cc.success(); } });
     * 
     * }
     */
    @ReactMethod
    private void removeKeyPair(final String user, final Promise promise) {
        if (nabto == null) {
            promise.reject(Integer.toString(NabtoStatus.API_NOT_INITIALIZED.ordinal()),
                    String.valueOf(NabtoStatus.API_NOT_INITIALIZED));
                    return;
        }
        NabtoStatus status = nabto.removeProfile(user);
        if (status != NabtoStatus.OK) {
            promise.reject(Integer.toString(status.ordinal()), String.valueOf(status));
            return;
        }
        promise.resolve(true);
        return;
    }

    /*
     * private void getFingerprint(final String certId, final CallbackContext cc){
     * cordova.getThreadPool().execute(new Runnable() {
     * 
     * @Override public void run(){ if (nabto == null) {
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; } String[]
     * fingerprint = new String[1]; fingerprint[0] = ""; NabtoStatus status =
     * nabto.getFingerprint(certId,fingerprint); if (status != NabtoStatus.OK) {
     * cc.error(status.ordinal()); return; } cc.success(fingerprint[0]); } }); }
     */
    @ReactMethod
    private void getFingerprint(final String certId, final Promise promise) {
        if (nabto == null) {
            promise.reject(Integer.toString(NabtoStatus.API_NOT_INITIALIZED.ordinal()),
                    String.valueOf(NabtoStatus.API_NOT_INITIALIZED));
                    return;
        }
        String[] fingerprint = new String[1];
        fingerprint[0] = "";
        NabtoStatus status = nabto.getFingerprint(certId, fingerprint);
        if (status != NabtoStatus.OK) {
            promise.reject(Integer.toString(status.ordinal()), String.valueOf(status));
            return;
        }
        promise.resolve(fingerprint[0]);
        return;
    }

    /*
     * private void shutdown(final CallbackContext cc) {
     * cordova.getThreadPool().execute(new Runnable() {
     * 
     * @Override public void run() { if(nabto != null){ nabto.shutdown(); } nabto =
     * null; session = null; deviceCache.clear(); adShown = false; cc.success(); }
     * }); }
     */
    @ReactMethod
    private void shutdown(final Promise promise) {
        if (nabto != null) {
            nabto.shutdown();
        }
        nabto = null;
        session = null;
        deviceCache.clear();
        adShown = false;
        promise.resolve(true);
        return;
    }

    /*
     * private void setBasestationAuthJson(final String authJson, final
     * CallbackContext cc) { final Context context =
     * cordova.getActivity().getApplicationContext();
     * cordova.getThreadPool().execute(new Runnable() {
     * 
     * @Override public void run() { if (session == null) {
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; } String json;
     * if (authJson.length() > 0) { json = authJson; } else { // we interpret
     * javascript empty string as user's intention of resetting auth data json =
     * null; } NabtoStatus status = nabto.setBasestationAuthJson(json, session); if
     * (status != NabtoStatus.OK) { cc.error(status.ordinal()); return; }
     * cc.success(); } });
     * 
     * }
     * 
     */
    @ReactMethod
    private void setBasestationAuthJson(final String authJson, final Promise promise) {
        if (session == null) {
            promise.reject(Integer.toString(NabtoStatus.API_NOT_INITIALIZED.ordinal()),
                    String.valueOf(NabtoStatus.API_NOT_INITIALIZED));
                    return;
        }
        String json;
        if (authJson.length() > 0) {
            json = authJson;
        } else {
            // we interpret javascript empty string as user's intention of resetting auth
            // data
            json = null;
        }
        NabtoStatus status = nabto.setBasestationAuthJson(json, session);
        if (status != NabtoStatus.OK) {
            promise.reject(Integer.toString(status.ordinal()), String.valueOf(status));
            return;
        }
        promise.resolve(true);
        return;
    }

    /*
     * private void setStaticResourceDir(final String dir, final CallbackContext cc)
     * { final Context context = cordova.getActivity().getApplicationContext();
     * cordova.getThreadPool().execute(new Runnable() {
     * 
     * @Override public void run() { if (nabto == null) {
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; } NabtoStatus
     * status = nabto.setStaticResourceDir(dir); if (status != NabtoStatus.OK) {
     * cc.error(status.ordinal()); return; } cc.success(); } });
     * 
     * }
     * 
     */
    @ReactMethod
    private void setStaticResourceDir(final String dir, final Promise promise) {
        if (nabto == null) {
            promise.reject(Integer.toString(NabtoStatus.API_NOT_INITIALIZED.ordinal()),
                    String.valueOf(NabtoStatus.API_NOT_INITIALIZED));
                    return;
        }
        NabtoStatus status = nabto.setStaticResourceDir(dir);
        if (status != NabtoStatus.OK) {
            promise.reject(Integer.toString(status.ordinal()), String.valueOf(status));
            return;
        }
        promise.resolve(true);
        return;
    }

    /*
     * private void fetchUrl(final String url, final CallbackContext cc) {
     * cordova.getThreadPool().execute(new Runnable() {
     * 
     * @Override public void run() { if (session == null) {
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; } UrlResult
     * result = nabto.fetchUrl(url, session); if (result.getStatus() !=
     * NabtoStatus.OK) { cc.error(result.getStatus().ordinal()); return; }
     * 
     * try { String stringResult = new String(result.getResult(), "UTF-8");
     * cc.success(stringResult); } catch (UnsupportedEncodingException e) {
     * cc.error("Nabto request parse error"); } } }); }
     * 
     */
    @ReactMethod
    private void fetchUrl(final String url, final Promise promise) {
        if (session == null) {
            promise.reject(Integer.toString(NabtoStatus.API_NOT_INITIALIZED.ordinal()),
                    String.valueOf(NabtoStatus.API_NOT_INITIALIZED));
                    return;
        }
        UrlResult result = nabto.fetchUrl(url, session);
        NabtoStatus status = result.getStatus();
        if (status != NabtoStatus.OK) {
            promise.reject(Integer.toString(status.ordinal()), String.valueOf(status));
            return;
        }

        try {
            String stringResult = new String(result.getResult(), "UTF-8");
            promise.resolve(stringResult);
            return;
        } catch (UnsupportedEncodingException e) {
            promise.reject("Nabto request parse error");
            return;
        }
    }

    /*
     * private void rpcInvoke(final String url, final CallbackContext cc) {
     * cordova.getThreadPool().execute(new Runnable() {
     * 
     * @Override public void run() { if (session == null) {
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; }
     * 
     * // looking up the device in the Cache. String dev = url.split("/")[2];
     * //Log.d("rpcInvoke","dev from URL: " + dev); boolean devKnown = false; for
     * (int i = 0; i < deviceCache.size(); i ++){ //Log.d("rpcInvoke","checking: " +
     * deviceCache.get(i)); if(deviceCache.get(i).equals(dev)){ devKnown = true;
     * break; } } // If the device is unknown rpcInvoke fails if(!devKnown){
     * JSONObject error = new JSONObject(); JSONObject root = new JSONObject(); try{
     * error.put("event",NABTO_ERROR_MISSING_PREPARE);
     * error.put("header","Unprepared device invoked"); error.put("detail", dev);
     * error.put("body","rpcInvoke was called with unprepared device: " + dev +
     * ". prepareInvoke must be called before device can be invoked");
     * root.put("error",error); } catch (JSONException e){
     * Log.e("rpcInvoke","could not put JSON error message");
     * cc.error(NabtoStatus.FAILED.ordinal()); return; }
     * //Log.w("rpcInvoke","root: " + root.toString()); cc.error(root.toString());
     * return; } RpcResult result = nabto.rpcInvoke(url, session); if
     * (result.getStatus() != NabtoStatus.OK) { if(result.getStatus() ==
     * NabtoStatus.FAILED_WITH_JSON_MESSAGE){ cc.error(result.getJson()); } else {
     * cc.error(result.getStatus().ordinal()); } return; }
     * 
     * String stringResult = new String(result.getJson()); cc.success(stringResult);
     * } }); }
     */
    @ReactMethod
    private void rpcInvoke(final String url, final Promise promise) {
        if (session == null) {
            promise.reject(Integer.toString(NabtoStatus.API_NOT_INITIALIZED.ordinal()),
                    String.valueOf(NabtoStatus.API_NOT_INITIALIZED));
            return;
        }

        // looking up the device in the Cache.
        String dev = url.split("/")[2];
        // Log.d("rpcInvoke","dev from URL: " + dev);
        boolean devKnown = false;
        for (int i = 0; i < deviceCache.size(); i++) {
            // Log.d("rpcInvoke","checking: " + deviceCache.get(i));
            if (deviceCache.get(i).equals(dev)) {
                devKnown = true;
                break;
            }
        }
        // If the device is unknown rpcInvoke fails
        if (!devKnown) {
            JSONObject error = new JSONObject();
            JSONObject root = new JSONObject();
            try {
                error.put("event", NABTO_ERROR_MISSING_PREPARE);
                error.put("header", "Unprepared device invoked");
                error.put("detail", dev);
                error.put("body", "rpcInvoke was called with unprepared device: " + dev
                        + ". prepareInvoke must be called before device can be invoked");
                root.put("error", error);
            } catch (JSONException e) {
                Log.e("rpcInvoke", "could not put JSON error message");
                promise.reject(Integer.toString(NabtoStatus.FAILED.ordinal()), String.valueOf(NabtoStatus.FAILED));
                return;
            }
            // Log.w("rpcInvoke","root: " + root.toString());
            promise.reject(Integer.toString(NabtoStatus.FAILED.ordinal()), root.toString());
            return;
        }
        RpcResult result = nabto.rpcInvoke(url, session);
        NabtoStatus status = result.getStatus();
        if (status != NabtoStatus.OK) {
            if (status == NabtoStatus.FAILED_WITH_JSON_MESSAGE) {
                promise.reject(Integer.toString(status.ordinal()), result.getJson());
                return;
            } else {
                promise.reject(Integer.toString(status.ordinal()), String.valueOf(status.ordinal()));
                return;
            }
        }

        String stringResult = new String(result.getJson());
        promise.resolve(stringResult);
        return;
    }

    /*
     * private void rpcSetDefaultInterface(final String interfaceXml, final
     * CallbackContext cc){ cordova.getThreadPool().execute(new Runnable() { public
     * void run() { if (session == null){
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; } RpcResult
     * result = nabto.rpcSetDefaultInterface(interfaceXml, session);
     * if(result.getStatus() == NabtoStatus.API_NOT_INITIALIZED){
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; } cc.success();
     * } });
     * 
     * }
     */
    @ReactMethod
    private void rpcSetDefaultInterface(final String interfaceXml, final Promise promise) {
        if (session == null) {
            promise.reject(Integer.toString(NabtoStatus.API_NOT_INITIALIZED.ordinal()),
                    String.valueOf(NabtoStatus.API_NOT_INITIALIZED));
            return;
        }
        RpcResult result = nabto.rpcSetDefaultInterface(interfaceXml, session);
        if (result.getStatus() == NabtoStatus.API_NOT_INITIALIZED) {
            promise.reject(Integer.toString(NabtoStatus.API_NOT_INITIALIZED.ordinal()),
                    String.valueOf(NabtoStatus.API_NOT_INITIALIZED));
                    return;
        }
        promise.resolve(true);
        return;
    }

    /*
     * private void rpcSetInterface(final String host, final String interfaceXml,
     * final CallbackContext cc){ cordova.getThreadPool().execute(new Runnable(){
     * public void run() { if (session == null) {
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; } RpcResult
     * result = nabto.rpcSetInterface(host,interfaceXml, session);
     * if(result.getStatus() == NabtoStatus.API_NOT_INITIALIZED){
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; } cc.success();
     * } });
     * 
     * }
     */
    @ReactMethod
    private void rpcSetInterface(final String host, final String interfaceXml, final Promise promise) {
        if (session == null) {
            promise.reject(Integer.toString(NabtoStatus.API_NOT_INITIALIZED.ordinal()),
                    String.valueOf(NabtoStatus.API_NOT_INITIALIZED));
                    return;
        }
        RpcResult result = nabto.rpcSetInterface(host, interfaceXml, session);
        if (result.getStatus() == NabtoStatus.API_NOT_INITIALIZED) {
            promise.reject(Integer.toString(NabtoStatus.API_NOT_INITIALIZED.ordinal()),
                    String.valueOf(NabtoStatus.API_NOT_INITIALIZED));
                    return;
        }
        promise.resolve(true);
        return;
    }

    /*
     * private void getSessionToken(final CallbackContext cc) {
     * cordova.getThreadPool().execute(new Runnable() {
     * 
     * @Override public void run() { if (nabto == null) {
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; } String token =
     * nabto.getSessionToken(session); cc.success(token); } }); }
     */
    @ReactMethod
    private void getSessionToken(final Promise promise) {
        if (nabto == null) {
            promise.reject(Integer.toString(NabtoStatus.API_NOT_INITIALIZED.ordinal()),
                    String.valueOf(NabtoStatus.API_NOT_INITIALIZED));
                    return;
        }
        String token = nabto.getSessionToken(session);
        promise.resolve(token);
        return;
    }

    /*
     * private void startupAndOpenProfile(final String user, final String pass,
     * final CallbackContext cc) { Log.d("startupAndOpenProfile",
     * "Nabto startupAndOpenProfile begins"); final Context context =
     * cordova.getActivity().getApplicationContext();
     * 
     * cordova.getThreadPool().execute(new Runnable() {
     * 
     * @Override public void run() { if (nabto == null){ nabto = new NabtoApi(new
     * NabtoAndroidAssetManager(context)); } NabtoStatus status = nabto.startup();
     * if (status != NabtoStatus.OK) { cc.error(status.ordinal()); return; }
     * 
     * if (session != null) { cc.success(); return; }
     * 
     * session = nabto.openSession(user, pass);
     * 
     * if (session.getStatus() != NabtoStatus.OK) {
     * cc.error(session.getStatus().ordinal()); session = null; } else {
     * cc.success(); } } }); }
     */
    @ReactMethod
    private void startupAndOpenProfile(final String user, final String pass, final Promise promise) {
        Log.d("startupAndOpenProfile", "Nabto startupAndOpenProfile begins");
        final Context context = getReactApplicationContext();

        if (nabto == null) {
            nabto = new NabtoApi(new NabtoAndroidAssetManager(context));
        }
        NabtoStatus status = nabto.startup();
        if (status != NabtoStatus.OK) {
            promise.reject(Integer.toString(status.ordinal()), String.valueOf(status));
            return;
        }

        if (session != null) {
            promise.resolve(true);
            return;
        }

        session = nabto.openSession(user, pass);
        status = session.getStatus();
        if (status != NabtoStatus.OK) {
            session = null;
            promise.reject(Integer.toString(status.ordinal()), String.valueOf(status));
            return;
        } else {
            promise.resolve(true);
            return;
        }
    }

    /*
     * private void startup(final CallbackContext cc) { Log.d("startup",
     * "Nabto startup begins"); final Context context =
     * cordova.getActivity().getApplicationContext();
     * cordova.getThreadPool().execute(new Runnable() {
     * 
     * @Override public void run() { if (nabto != null) { Log.d("startup",
     * "Nabto was already started"); cc.success(); return; } nabto = new
     * NabtoApi(new NabtoAndroidAssetManager(context)); nabto.startup();
     * Log.d("startup", "Nabto started"); cc.success(); } }); }
     * 
     * 
     */
    @ReactMethod
    private void startup(final Promise promise) {
        Log.d("startup", "Nabto startup begins");
        final Context context = getReactApplicationContext();
        if (nabto != null) {
            Log.d("startup", "Nabto was already started");
            promise.resolve(true);
            return;
        }
        nabto = new NabtoApi(new NabtoAndroidAssetManager(context));
        nabto.startup();
        promise.resolve(true);
    }

    /*
     * 
     * private void getLocalDevices(final CallbackContext cc) {
     * cordova.getThreadPool().execute(new Runnable() {
     * 
     * @Override public void run() { if (nabto == null) {
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; }
     * Collection<String> devices = nabto.getLocalDevices(); JSONArray jsonArray =
     * new JSONArray(devices); //Arrays.asList(devices)); cc.success(jsonArray); }
     * }); }
     */
    @ReactMethod
    private void getLocalDevices(final Promise promise) {
        if (nabto == null) {
            promise.reject(Integer.toString(NabtoStatus.API_NOT_INITIALIZED.ordinal()),
                    new Throwable(String.valueOf(NabtoStatus.API_NOT_INITIALIZED)));
                    return;
        }
        Collection<String> devices = nabto.getLocalDevices();
        WritableArray returnDevices = new WritableNativeArray();
        for (String device : devices) {
            returnDevices.pushString(device);
        }
        promise.resolve(returnDevices);
        return;
    }

    /*
     * 
     * 
     * private void version(final CallbackContext cc) {
     * cordova.getThreadPool().execute(new Runnable() {
     * 
     * @Override public void run() { if (nabto == null) {
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; } String version
     * = nabto.version(); cc.success(version); } }); }
     */
    /*@ReactMethod
    private void version(final Promise promise) {
        if (nabto == null) {
            promise.reject(Integer.toString(NabtoStatus.API_NOT_INITIALIZED.ordinal()),
                    String.valueOf(NabtoStatus.API_NOT_INITIALIZED.ordinal()));
                    return;
        }
        promise.resolve(nabto.version());
        return;
    }*/

    /*
    
    */
    @ReactMethod
    public void versionString(final Promise promise) {
        if (nabto == null) {
            promise.reject(Integer.toString(NabtoStatus.API_NOT_INITIALIZED.ordinal()),
                    String.valueOf(NabtoStatus.API_NOT_INITIALIZED.ordinal()));
                    return;
        }
        promise.resolve(nabto.versionString());
        return;
    }

    /*
     * private void setOption(final String key, final String value, final
     * CallbackContext cc) { cordova.getThreadPool().execute(new Runnable() {
     * 
     * @Override public void run() { if (nabto == null) {
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; } NabtoStatus
     * status = nabto.setOption(key, value); if (status == NabtoStatus.OK) {
     * cc.success(); } else { cc.error(status.ordinal()); return; } } }); }
     * 
     */
    @ReactMethod
    private void setOption(final String key, final String value, final Promise promise) {
        if (nabto == null) {
            promise.reject(Integer.toString(NabtoStatus.API_NOT_INITIALIZED.ordinal()),
                    String.valueOf(NabtoStatus.API_NOT_INITIALIZED.ordinal()));
                    return;
        }
        NabtoStatus status = nabto.setOption(key, value);
        if (status == NabtoStatus.OK) {
            promise.resolve(true);
            return;
        } else {
            promise.reject(Integer.toString(status.ordinal()), String.valueOf(status));
            return;
        }
    }

    /* Nabto Tunnel API */
    /*
     * private void tunnelOpenTcp(final String host, final int port, final
     * CallbackContext cc) { cordova.getThreadPool().execute(new Runnable() {
     * 
     * @Override public void run() { if (nabto == null) {
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; } Tunnel tunnel
     * = nabto.tunnelOpenTcp(0, host, "localhost", port, session); NabtoStatus
     * status = tunnel.getStatus(); if (status == NabtoStatus.OK) { TunnelInfoResult
     * info = nabto.tunnelInfo(tunnel); while (info.getStatus() == NabtoStatus.OK &&
     * info.getTunnelState() == NabtoTunnelState.CONNECTING) { try {
     * Thread.sleep(100); } catch (InterruptedException e) { // ignore } info =
     * nabto.tunnelInfo(tunnel); } if (info.getStatus() == NabtoStatus.OK &&
     * info.getTunnelState() != NabtoTunnelState.CLOSED) { String handle =
     * tunnel.getHandle().toString(); tunnels.put(handle, tunnel);
     * cc.success(handle); } else { if (info.getStatus() == NabtoStatus.OK) { //
     * json error message instead (currently not possible for caller to // determine
     * domain (api or p2p error)) cc.error(NabtoStatus.INVALID_TUNNEL.ordinal()); }
     * else { cc.error(info.getStatus().ordinal()); } } } else {
     * cc.error(status.ordinal()); } } }); }
     * 
     * private void tunnelVersion(final String tunnelHandle, final CallbackContext
     * cc) { cordova.getThreadPool().execute(new Runnable() {
     * 
     * @Override public void run() { if (nabto == null) {
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; } Tunnel tunnel
     * = tunnels.get(tunnelHandle); if (tunnel != null) { TunnelInfoResult info =
     * nabto.tunnelInfo(tunnel); if (info.getStatus() == NabtoStatus.OK) {
     * cc.success(info.getVersion()); } else {
     * cc.error(NabtoStatus.FAILED.ordinal()); } } else {
     * cc.error(NabtoStatus.INVALID_TUNNEL.ordinal()); } } }); }
     * 
     * private void tunnelState(final String tunnelHandle, final CallbackContext cc)
     * { cordova.getThreadPool().execute(new Runnable() {
     * 
     * @Override public void run() { if (nabto == null) {
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; } Tunnel tunnel
     * = tunnels.get(tunnelHandle); if (tunnel != null) { TunnelInfoResult info =
     * nabto.tunnelInfo(tunnel); if (info.getStatus() == NabtoStatus.OK) {
     * cc.success(info.getTunnelState().ordinal()); } else {
     * cc.error(NabtoStatus.FAILED.ordinal()); } } else {
     * cc.error(NabtoStatus.INVALID_TUNNEL.ordinal()); } } }); }
     * 
     * private void tunnelLastError(final String tunnelHandle, final CallbackContext
     * cc) { cordova.getThreadPool().execute(new Runnable() {
     * 
     * @Override public void run() { if (nabto == null) {
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; } Tunnel tunnel
     * = tunnels.get(tunnelHandle); if (tunnel != null) { TunnelInfoResult info =
     * nabto.tunnelInfo(tunnel); if (info.getStatus() == NabtoStatus.OK) {
     * cc.success(info.getLastError()); } else {
     * cc.error(NabtoStatus.FAILED.ordinal()); } } else {
     * cc.error(NabtoStatus.INVALID_TUNNEL.ordinal()); } } }); }
     * 
     * private void tunnelPort(final String tunnelHandle, final CallbackContext cc)
     * { cordova.getThreadPool().execute(new Runnable() {
     * 
     * @Override public void run() { if (nabto == null) {
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; } Tunnel tunnel
     * = tunnels.get(tunnelHandle); if (tunnel != null) { TunnelInfoResult info =
     * nabto.tunnelInfo(tunnel); if (info.getStatus() == NabtoStatus.OK) {
     * cc.success(info.getPort()); } else { cc.error(NabtoStatus.FAILED.ordinal());
     * } } else { cc.error(NabtoStatus.INVALID_TUNNEL.ordinal()); } } }); }
     * 
     * private void tunnelClose(final String tunnelHandle, final CallbackContext cc)
     * { cordova.getThreadPool().execute(new Runnable() {
     * 
     * @Override public void run() { if (nabto == null) {
     * cc.error(NabtoStatus.API_NOT_INITIALIZED.ordinal()); return; } Tunnel tunnel
     * = tunnels.get(tunnelHandle); if (tunnel != null) { NabtoStatus status =
     * nabto.tunnelClose(tunnel); if (status == NabtoStatus.OK) {
     * tunnels.remove(tunnel); cc.success(); } else { cc.error(status.ordinal()); }
     * } else { cc.error(NabtoStatus.INVALID_TUNNEL.ordinal()); return; } } }); }
     */
}
