
# react-native-nabto

## Getting started

`$ npm install react-native-nabto --save`

### Mostly automatic installation

`$ react-native link react-native-nabto`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-nabto` and add `RNNabto.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNNabto.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNNabtoPackage;` to the imports at the top of the file
  - Add `new RNNabtoPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-nabto'
  	project(':react-native-nabto').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-nabto/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-nabto')
  	```

#### Windows
[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNNabto.sln` in `node_modules/react-native-nabto/windows/RNNabto.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app
  - Add `using Nabto.RNNabto;` to the usings at the top of the file
  - Add `new RNNabtoPackage()` to the `List<IReactPackage>` returned by the `Packages` method


## Usage
```javascript
import RNNabto from 'react-native-nabto';

// TODO: What to do with the module?
RNNabto;
```
  